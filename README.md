Algunas cosas para tener en cuenta respecto al storage account

si se crea un proyecto desde cero deben instalarse dos librerias para poder trabajar.

npm install @azure/storage-blob --save
npm install @azure/identity --save

lugares donde ingresar el sas (blobContainer-storage.service y home.component.ts)
lugares donde ingresar el nombre de tu cuenta de almacenamiento (blobContainer-storage.service y blobFile-sotrage.service)


1.- esta app trabaja solo con un tipo de sas general generado al nivel del la cuenta de almacenamiento creada las instrucciones para crearlo estan a continuacion:

Para obtener este sas tienes que ir a la cuenta de almacenamiento que se creo y buscar donde dice Firma de acceso compartido en los tipos de recursos permitidos selecciona los 3, Servicio contenedor y objeto, luego defines la caducidad del sas y finalmente generas la cadena de coneccion y el sas.


2.- Los nombres de los archivos no se pueden repetir
3.- Los nombres de los containers no se pueden repetir
4.- No se puede crear un container dentro de otro container


5.- Observaciones sobre los nombres de los containers(carpetas)

Los nombres de los contenedores deben comenzar o terminar con una letra o un número, y solo pueden contener letras, números y el carácter de guion (-).

Cada carácter de guion (-) debe ir inmediatamente precedido y seguido de una letra o un número; no se permiten guiones consecutivos en los nombres de los contenedores.

Todas las letras del nombre de un contenedor deben estar en minúsculas.

Los nombres de los contenedores deben tener entre 3 y 63 caracteres.


 
6.- Observaciones sobre los nombres de los Blobs(archivos)

Un nombre de blob puede contener cualquier combinación de caracteres.

El nombre de un blob debe tener al menos un carácter y no puede tener más de 1024 caracteres, para blobs en Azure Storage.

Los nombres de los blobs distinguen entre mayúsculas y minúsculas.

Los caracteres de URL reservados deben escaparse correctamente.

Si su cuenta no tiene un espacio de nombres jerárquico, la cantidad de segmentos de ruta que comprende el nombre del blob no puede exceder 254. Un segmento de ruta es la cadena entre caracteres delimitadores consecutivos (por ejemplo, la barra inclinada '/') que corresponde al nombre de un directorio virtual