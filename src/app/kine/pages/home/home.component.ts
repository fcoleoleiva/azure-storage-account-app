import { Component, OnInit } from '@angular/core';
import { ContainerCreateOptions } from '@azure/storage-blob';
import { BlobContainerStorageService } from 'src/app/service/blobContainer-storage.service';
import { BlobFileStorageService } from 'src/app/service/blobFile-storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent {

  constructor(private blob:BlobContainerStorageService,private blobService: BlobFileStorageService){}

  //ingresa aqui el sas generado
  sas = "ingresa tu sas aqui";

  containerName: string = ""
  files:any
  fileList: string[] = [];
  filesDownloaded: string[] = []

  create(){
    this.blob.containerSAS(this.containerName)
    }

  delete(){
      this.blob.deleteContainer(this.containerName)
    }

  listar(){
      this.blob.listarContainers()
    }

  listarFotos(){
    this.blobService.GetContainerName(this.containerName)
    this.reloadImages()
    }

  public imageSelected(event:any):void{
      this.files = event.target.files
  }

  public loadFile():void{
    this.blobService.uploadImage(this.sas, this.files[0], this.files[0].name, () => {
    this.reloadImages()
  })
}

  public deleteImage (name: string) {
    this.blobService.deleteImage(this.sas, name, () => {
      this.reloadImages()
    })
  }

  public downloadImage (name: string) {
    this.blobService.downloadImage(this.sas, name, blob => {
      let url = window.URL.createObjectURL(blob);
      window.open(url);
    })
  }

  private reloadImages() {
    this.blobService.listImages(this.sas).then(list => {
      this.fileList = list
      const array:any = []
      this.filesDownloaded = array

      for (let name of this.fileList) {
        this.blobService.downloadImage(this.sas, name, blob => {
          var reader = new FileReader();
          reader.readAsDataURL(blob);
          reader.onloadend = function () {
            array.push(reader.result as string)
          }
        })
      }
    })
  }

}
