import { Injectable } from '@angular/core';
import { BlobServiceClient, ContainerCreateOptions, ContainerCreateResponse } from '@azure/storage-blob';

@Injectable({
  providedIn: 'root'
})
export class BlobContainerStorageService {
  
  //ingresa aqui el nombre de la cuenta de almacenamiento creado en azure
  account =  "ingresa el nombre de tu cuenta de almacenamiento aqui";
  //ingresa aqui el sas generado, mas informacion en el home.component.ts se pega aca para crear o eliminar un Container
  accountSas = "ingrea tu sas aqui";





  async containerSAS(containerName:string){
    try {
      const blobServiceClient = new BlobServiceClient(`https://${this.account}.blob.core.windows.net${this.accountSas}`);
      const options: ContainerCreateOptions = { //define el nivel de acceso de un container en container lo que sigifica que es publico
        access: 'container'
      };
      const {
        containerCreateResponse
      }: {
        containerCreateResponse: ContainerCreateResponse;
      } = await blobServiceClient.createContainer(containerName, options);
      console.log(`Container ${containerName} Creado con exito---> etag ${containerCreateResponse.etag}`);
    // const containerClient = blobServiceClient.createContainer(containerName,options) Tambien puedes usar este formato para crear el container
      
    } catch (error) {
      console.error('Se produjo un error:', error);
      
    }

  }

  async listarContainers(){
    try {
      const blobServiceClient = new BlobServiceClient(`https://${this.account}.blob.core.windows.net${this.accountSas}`);
      console.log("Containers:");
      for await (const container of blobServiceClient.listContainers()) {
        console.log(`- ${container.name}`);
      }    
    } catch (error) {
      console.error('Se produjo un error:', error);
    }
  }

  async deleteContainer(containerName: string){
    try {
      const blobServiceClient = new BlobServiceClient(`https://${this.account}.blob.core.windows.net${this.accountSas}`);
      await blobServiceClient.deleteContainer(containerName);
      console.log(`Container ${containerName} Eliminado con exito`)
    } catch (error) {
      console.error('Se produjo un error:', error);
    }
  }

}
