import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { MaterialIcon } from 'material-icons';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { KineModule } from './kine/kine.module';


@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    BrowserAnimationsModule,
    KineModule,

    
  ],
  exports: [

    
  ],
  providers: [],
  
  bootstrap: [AppComponent]
})
export class AppModule { }

